#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
ZetCode Tkinter tutorial

In this script, we use pack manager
to position two buttons in the
bottom right corner of the window. 

author: Jan Bodnar
last modified: December 2010
website: www.zetcode.com
"""
import os
import glob
from Tkinter import *
import tkMessageBox as box

class Book:
    Title = ""
    Author_First = ""
    Author_Last = ""
    Series = ""
    Date = ""
    Genre = ""

    def Check_for_Missing(self):
        if self.Title == "" or self.Author_First == "" or self.Author_Last == "" or self.Series == "" or self.Date == "" or self.Genre == "":
            return 1
        else:
            return 0


class Example(Frame):
  
    def __init__(self, parent):
        Frame.__init__(self, parent)   
         
        self.parent = parent

        #Get the location of the book records from a file in this directory
        fid = open("Records_Dir.txt",'r')
        self.Records_Dir = fid.readline()
        
        self.initUI()
        
    def initUI(self):
      
        self.parent.title("Buttons")
        #self.grid( sticky = W+E+N+S )
        FL = Frame(self.parent)
        FL.grid(sticky = W+E+N+S)
        FL.pack(side=LEFT, fill=Y)
        FR = Frame(self.parent)
        FR.grid(sticky = W+E+N+S)
        FR.pack(side=RIGHT)
        
        #First column
        w = Label(FL, text="Authors")
        w.grid(row=0, column=0)

        AuthorSB = Scrollbar(FL)
        AuthorSB.grid(row=1, column=1, sticky="ns")
                
        self.AuthorLB = Listbox(FL, yscrollcommand=AuthorSB.set, width=30)
        self.AuthorLB.grid(row=1, column=0, padx=(10,0))
        self.AuthorLB.bind("<<ListboxSelect>>", self.onAuthorSelect)
        self.AuthorLB_LastIndex = 0
        self.AuthorLB_LastSearchString = ""
        
        AuthorSB.config(command=self.AuthorLB.yview)
        
                
        #Second column
        w = Label(FL, text="Series")
        w.grid(row=0, column=2)

        SeriesSB = Scrollbar(FL)
        SeriesSB.grid(row=1, column=3, sticky="ns")
        
        self.SeriesLB = Listbox(FL, yscrollcommand=SeriesSB.set, width=30)
        self.SeriesLB.grid(row=1, column=2, padx=(10,0))
        self.SeriesLB.bind("<<ListboxSelect>>", self.onSeriesSelect)
        self.SeriesLB_LastIndex = 0
        self.SeriesLB_LastSearchString = ""
        
        SeriesSB.config(command=self.SeriesLB.yview)
        
        
        #Third Column
        w = Label(FL, text="Books")
        w.grid(row=0, column=4)

        BooksSB = Scrollbar(FL)
        BooksSB.grid(row=1, column=5, sticky="nsw")
        
        self.BooksLB = Listbox(FL, yscrollcommand=BooksSB.set, width=30)
        self.BooksLB.grid(row=1, column=4, padx=(10,0))
        self.BooksLB.bind("<<ListboxSelect>>", self.onBooksSelect)
        
        BooksSB.config(command=self.BooksLB.yview)

        w = Label(FL, text="If book is unread, enter '0' for date. If book is read and date is unknown, enter '9' for date.")
        w.grid(row=3, column=0, columnspan=6, padx=(10,0), pady=(10,10), sticky="w")

        self.NumberRead = StringVar()
        self.Search_Count_Read()

        w = Label(FL, textvariable=self.NumberRead)
        w.grid(row=4, column=0, columnspan=6, padx=(10,0), pady=(0,10), sticky="w")
        
        #New Book Columns
        w = Label(FR, text="New Book Entry")
        w.grid(row=0, column=0, columnspan=2, pady=(10,5))
        
        w = Label(FR, text="First Name:")
        w.grid(row=1, column=0, padx=(10,5), pady=(0,5), sticky="e")
        
        self.Author_First_Str = StringVar()
        self.Author_First = Entry(FR, textvariable=self.Author_First_Str, width=30)
        self.Author_First.grid(row=1, column=1, padx=(0,10))
        
        w = Label(FR, text="Last Name:")
        w.grid(row=2, column=0, padx=(10,5), pady=(0,5), sticky="e")
        
        self.Author_Last_Str = StringVar()
        self.Author_Last = Entry(FR, textvariable=self.Author_Last_Str, width=30)
        self.Author_Last.grid(row=2, column=1, padx=(0,10))
        
        w = Label(FR, text="Title:")
        w.grid(row=3, column=0, padx=(10,5), pady=(0,5), sticky="e")
        
        self.New_Title_Str = StringVar()
        self.New_Title = Entry(FR, textvariable=self.New_Title_Str, width=30)
        self.New_Title.grid(row=3, column=1, padx=(0,10))
        
        w = Label(FR, text="Series:")
        w.grid(row=4, column=0, padx=(10,5), pady=(0,5), sticky="e")
        
        self.New_Series_Str = StringVar()
        self.New_Series = Entry(FR, textvariable=self.New_Series_Str, width=30)
        self.New_Series.grid(row=4, column=1, padx=(0,10))
        
        w = Label(FR, text="Date (YYYYMMDD):")
        w.grid(row=5, column=0, padx=(10,5), pady=(0,5), sticky="e")
        
        self.New_Date_Str = StringVar()
        self.New_Date = Entry(FR, textvariable=self.New_Date_Str, width=30)
        self.New_Date.grid(row=5, column=1, padx=(0,10))
        
        w = Label(FR, text="Genre:")
        w.grid(row=6, column=0, padx=(10,5), pady=(0,5), sticky="e")
        
        self.New_Genre_Str = StringVar()
        self.New_Genre = Entry(FR, textvariable=self.New_Genre_Str, width=30)
        self.New_Genre.grid(row=6, column=1, padx=(0,10))
        
        self.SaveButton = Button(FR, text="Save", command=self.onSaveClick)
        self.SaveButton.grid(row=7, column=0, columnspan=2, sticky="ns")    


        self.Series_Unread = [] 
        self.Populate_Authors()
        self.AuthorLB.selection_set(0)
        self.AuthorLB.focus_set()

    def Populate_Authors(self):
        self.AuthorLB.delete(0, last=END)
        self.Authors_Unread = []              
        Author_Number = 1
        self.Authors_Unread.append(0)
        os.chdir(self.Records_Dir)
        self.AuthorLB.insert(END, "")
        for FileName in glob.glob("*"):
            FileName_p = FileName.split("_")
            AuthorName =  FileName_p[0] + ", " + FileName_p[1]
            self.AuthorLB.insert(END, AuthorName)

            os.chdir(FileName)
            self.Authors_Unread.append(0)
            for SeriesName in glob.glob("*.txt"):
                SeriesName_p = SeriesName.split(".")
                fid = open(SeriesName,'r')
                for line in fid:
                    line_p = line.split(';')
                    if line_p[4] == "00000000\n":
                        self.Authors_Unread[-1] = 1
                        self.AuthorLB.itemconfig(Author_Number, background="white", foreground="red")
                    
                fid.close()
            os.chdir("..")
            Author_Number = Author_Number + 1
        os.chdir("..")
    
    def onAuthorSelect(self, val):
        #Clear the series list box
        self.SeriesLB.delete(0, last=END)
        self.BooksLB.delete(0, last=END)
        
        #Remove highlight from previous selection
        if self.Authors_Unread[self.AuthorLB_LastIndex] == 1:
            self.AuthorLB.itemconfig(self.AuthorLB_LastIndex, background="white", foreground="red")
        else:
            self.AuthorLB.itemconfig(self.AuthorLB_LastIndex, background="white", foreground="black")
        
        
        self.Series_Unread = [] 
        Index = self.AuthorLB.curselection()
        Index = int(Index[0])
        self.AuthorLB_LastIndex = Index
        if not Index == 0:
            Selected_Name = self.AuthorLB.get(Index)
            Selected_Name_p = Selected_Name.split(", ")
            SearchString = Selected_Name_p[0] + "_" + Selected_Name_p[1]
            self.AuthorLB_LastSearchString = SearchString
        
            os.chdir(self.Records_Dir)
            for FileName in glob.glob("*"):
                if FileName == SearchString:
                    os.chdir(FileName)
                    SeriesCount = 0
                    for SeriesName in glob.glob("*.txt"):
                        SeriesName_p = SeriesName.split(".")
                        self.SeriesLB.insert(END, SeriesName_p[0])
                       
                        self.Series_Unread.append(0) 
                        fid = open(SeriesName,'r')
                        for line in fid:
                            line_p = line.split(';')
                            if line_p[4] == "00000000\n":
                                self.SeriesLB.itemconfig(SeriesCount, background="white", foreground="red")
                                self.Series_Unread[-1] = 1
                            
                        fid.close()
                        SeriesCount = SeriesCount + 1
                    os.chdir("..")
            os.chdir("..")
            
        #Highlight the author row
        self.AuthorLB.itemconfig(Index, background="#1E90FF", foreground="white")
        
        #Fill in the New Entry blank
        if not Index == 0:
            self.Author_First_Str.set(Selected_Name_p[1])
            self.Author_Last_Str.set(Selected_Name_p[0])
        else:
            self.Author_First_Str.set("")
            self.Author_Last_Str.set("")
        self.New_Title_Str.set("")
        self.New_Date_Str.set("")
        self.New_Genre_Str.set("")
        self.New_Series_Str.set("")
            
    def onSeriesSelect(self, val):
    
        #Clear the series list box
        self.BooksLB.delete(0, last=END)
        
        #Remove highlight from previous selection
        if (self.SeriesLB.size()-1) >= self.SeriesLB_LastIndex:
            if self.Series_Unread[self.SeriesLB_LastIndex] == 1:
                self.SeriesLB.itemconfig(self.SeriesLB_LastIndex, background="white", foreground="red")
            else:
                self.SeriesLB.itemconfig(self.SeriesLB_LastIndex, background="white", foreground="black")

        
        
        Index = self.SeriesLB.curselection()
        Index = int(Index[0])
        Selected_Series = self.SeriesLB.get(Index)
        self.SeriesLB_LastIndex = Index
        self.SeriesLB_LastSearchString = Selected_Series        
                
        os.chdir(self.Records_Dir)
        SearchString = self.AuthorLB_LastSearchString
        for FileName in glob.glob("*"):
            if FileName == SearchString:
                os.chdir(FileName)
                for SeriesName in glob.glob("*.txt"):
                    SeriesName_p = SeriesName.split('.')
                    if SeriesName_p[0] == Selected_Series:
                    
                        fid = open(SeriesName,'r')
                        BookCount = 0
                        for line in fid:
                            line_p = line.split(';')
                            self.BooksLB.insert(END, line_p[0])
                            Genre = line_p[1]
                            if line_p[4] == "00000000\n":
                                self.BooksLB.itemconfig(BookCount, background="white", foreground="red")
                            BookCount = BookCount + 1
                        fid.close()
                    
                os.chdir("..")
        os.chdir("..")
        
        #Highlight the author row
        self.SeriesLB.itemconfig(Index, background="#1E90FF", foreground="white")
        self.New_Series_Str.set(Selected_Series)
        self.New_Title_Str.set("")
        self.New_Date_Str.set("")
        self.New_Genre_Str.set(Genre)
        
    def onBooksSelect(self, val):
    
        Index = self.BooksLB.curselection()
        Index = int(Index[0])
        Selected_Book = self.BooksLB.get(Index)
        
        os.chdir(self.Records_Dir)
        SearchString = self.AuthorLB_LastSearchString
        for FileName in glob.glob("*"):
            if FileName == SearchString:
                os.chdir(FileName)
                for SeriesName in glob.glob("*.txt"):
                    SeriesName_p = SeriesName.split('.')
                    if SeriesName_p[0] == self.SeriesLB_LastSearchString:
                    
                        fid = open(SeriesName,'r')
                        for line in fid:
                            line_p = line.split(';')
                            if line_p[0] == Selected_Book:
                                self.New_Title_Str.set(Selected_Book)
                                self.New_Date_Str.set(line_p[4])
                                self.New_Genre_Str.set(line_p[1])
                        
                        fid.close()
                    
                os.chdir("..")
        os.chdir("..")
        
        
    def onSaveClick(self):
        os.chdir(self.Records_Dir)
        NewEntry = Book()
        
        NewEntry.Title = self.New_Title_Str.get()
        NewEntry.Author_First = self.Author_First_Str.get()
        NewEntry.Author_Last = self.Author_Last_Str.get()
        NewEntry.Series = self.New_Series_Str.get()
        NewEntry.Date = self.New_Date_Str.get()
        NewEntry.Genre = self.New_Genre_Str.get()

        if NewEntry.Check_for_Missing():
            box.showerror("Error", "One or more of the book entries is blank.")
            return

        if NewEntry.Date == "0":
            NewEntry.Date = "00000000"

        if NewEntry.Date == "9":
            NewEntry.Date = "99999999"

        SearchString = NewEntry.Author_Last + "_" + NewEntry.Author_First
        NewAuthor = 1
        for FileName in glob.glob("*"):
            if FileName == SearchString:
                os.chdir(FileName)
                NewAuthor = 0
                break
        #### Easy one, make a new folder and entry
        if NewAuthor == 1:
            os.mkdir(SearchString)
            os.chdir(SearchString)
            fid = open(NewEntry.Series + ".txt",'w')
            fid.write(NewEntry.Title + ";" + NewEntry.Genre + ";" + NewEntry.Author_Last + ";"\
             + NewEntry.Author_First + ";" + NewEntry.Date + "\n")
            fid.close()
            os.chdir("..")
            os.chdir("..")
            self.AfterSave()
            return 
        
        #### Harder one, have to check if this entry exists 
        for FileName in glob.glob("*.txt"):
            ### Check if this series is already started 
            if FileName == (NewEntry.Series+".txt"):
                ### Yep, now check if this entry exists
                fid = open(FileName,'r')
                UserInput = "z"
                LineCount = 0
                for line in fid:
                    LineCount = LineCount + 1
                    Existing = line.split(';')
                    if Existing[0] == NewEntry.Title and Existing[1] == NewEntry.Genre:
                        UserInput = box.askquestion("Question", "This entry already exists, do you want to overwrite it?")
                        break
                fid.close()
                if UserInput == "no":
                    os.chdir("..")
                    os.chdir("..")
                    return
                elif UserInput == "yes":
                    LineCount2 = 0;
                    fid = open(FileName,'r')
                    OldData = fid.readlines()
                    fid.close
                    fid = open(FileName,'w')
                    for line in OldData:
                        LineCount2 = LineCount2 + 1
                        if LineCount2 == LineCount:
                            fid.write(NewEntry.Title + ";" + NewEntry.Genre + ";" + NewEntry.Author_Last\
                             + ";" + NewEntry.Author_First + ";" + NewEntry.Date + "\n")
                        else: 
                            fid.write(line)
                    fid.close
                    os.chdir("..")
                    os.chdir("..")
                    self.AfterSave()
                    return

                else:
                    fid = open(FileName,'a')
                    fid.write(NewEntry.Title + ";" + NewEntry.Genre + ";" + NewEntry.Author_Last\
                     + ";" + NewEntry.Author_First + ";" + NewEntry.Date + "\n")
                    fid.close()
                    os.chdir("..")
                    os.chdir("..")
                    self.AfterSave()
                    return

        ###If we're here, this series doesn't exist for this author
        fid = open(NewEntry.Series + ".txt",'w')
        fid.write(NewEntry.Title + ";" + NewEntry.Genre + ";" + NewEntry.Author_Last + ";"\
         + NewEntry.Author_First + ";" + NewEntry.Date + "\n")
        fid.close()
        os.chdir("..")
        os.chdir("..")
        self.AfterSave()
        return


    def AfterSave(self):
        self.Populate_Authors()
        self.AuthorLB.selection_set(self.AuthorLB_LastIndex)
        self.Search_Count_Read()
        #self.onAuthorSelect()
        #self.SeriesLB.selection_set(self.SeriesLB_LastIndex)
        #self.onSeriesSelect()


    def Search_Count_Read(self):
        BookCount = 0
        os.chdir(self.Records_Dir)
        for FileName in glob.glob("*"):
            os.chdir(FileName)
            for SeriesName in glob.glob("*.txt"):
                fid = open(SeriesName,'r')
                for line in fid:
                    
                    line_p = line.split(';')
                    if line_p[4] != "00000000\n":
                        BookCount = BookCount + 1
                fid.close()
            os.chdir("..")
        os.chdir("..")
        self.NumberRead.set( "Number of books read: " + str(BookCount))
    
    

def main():
  
    root = Tk()
    
    #root.geometry("300x200+300+300")
    app = Example(root)
    root.mainloop()  


if __name__ == '__main__':
    main()  
