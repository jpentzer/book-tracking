Book Tracking Program
Author: Jesse Pentzer

This is a small program I wrote to keep track of all the books I read for fun. The main reason I wrote it was because I was always starting series that I liked and then forgetting about them when the next book was no available at the library. The program creates a directory with folders for each author and text files for each of the book series entered under that author.

There are many areas for improvement if I ever had time.
1. Change from a text-file database to an actual database framework.
2. There are sum bugs in the GUI operation. Sometimes you the text files become corrupted with extra commas or blank lines. 